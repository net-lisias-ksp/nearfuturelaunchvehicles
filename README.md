# Near Future Launch Vehicles : UNOFFICIAL

This is a unofficial, non authorized repository for **Near Future Launch Vehicles** for historical reference and troubleshooting.


## In a Hurry

* [Binaries](https://github.com/net-lisias-ksph/NearFutureLaunchVehicles/tree/ARR/Archive)
* [Sources](https://github.com/net-lisias-ksph/NearFutureLaunchVehicles/tree/ARR/Master)
* [Change Log](./CHANGE_LOG.md)


## Description

The latest and greatest (for size, perhaps) in launch vehicle components. This pack includes

* **5.0m Parts**: a balanced and extensive part set that provides a new size of rocket parts. Comes with tanks, adapters, utility parts and specialized components for engine clustering.
* **7.5m Parts**: an extra-large set of rocket parts for those huge constructions. Includes adapters, clustering, cargo and utility parts.
* **Advanced Engines**: several new rocket engines based on a whole set of concepts in the 0.625 to 3.75m sizes.
* **Support Parts**: new supporting parts that help enhance the large rocket experience, like heavy RCS thrusters.

## License

This AddOn is (C) [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/). All Right Reserved.

This repository is under the claim of the [right to backup](https://info.legalzoom.com/copyright-law-making-personal-copies-22200.html) and should be considered private:

> Copyright law permits you to make one copy of your computer software for the purpose of archiving the software in case it is damaged or lost. In order to make a copy, you must own a valid copy of the software and must destroy or transfer your backup copy if you ever decide to sell, transfer or give away your original valid copy. It is not legal to sell a backup copy of software unless you are selling it along with the original.

I grant you no rights on any artifact on this repository, unless you own yourself the right to use the software and authorizes me to keep backups for you:

> (a) Making of Additional Copy or Adaptation by Owner of Copy. -- Notwithstanding the provisions of section 106, it is not an infringement for the owner of a copy of a computer program to make or authorize the making of another copy or adaptation of that computer program provided:
> 
>> (2) that such new copy or adaptation is for archival purposes only and that all archival copies are destroyed in the event that continued possession of the computer program should cease to be rightful.

[17 USC §117(a)](https://www.law.cornell.edu/uscode/text/17/117)

By using this repository without the required pre-requisites, I hold you liable to copyright infringement.


## References

* [Nertea](https://forum.kerbalspaceprogram.com/index.php?/profile/83952-nertea/)
	+ [KSP Forum](https://forum.kerbalspaceprogram.com/index.php?/topic/155465-17x-near-future-technologies-starting-17-era-updates/)
	+ [imgur](https://imgur.com/a/EYbmB)
	+ [CurseForge](https://www.curseforge.com/kerbal/ksp-mods/near-future-launch-vehicles)
	+ [SpaceDock](https://spacedock.info/mod/1434/Near%20Future%20Launch%20Vehicles)
	+ [GitHub](https://github.com/ChrisAdderley/NearFutureLaunchVehicles)
